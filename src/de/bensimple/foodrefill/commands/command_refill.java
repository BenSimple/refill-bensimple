package de.bensimple.foodrefill.commands;

import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class command_refill implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender cs, Command command, String s, String[] args) {
        if (cs instanceof Player) {
            Player p = (Player) cs;
            p.getName();
            System.out.println("Der Spieler " + p + " hat sich erfrischt");
            p.setHealth(p.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
            p.setFoodLevel(20);
            p.sendMessage("§2Dein Leben wurde aufgefüllt");
            p.sendMessage("§aund du hast keinen Hunger mehr");


        }
        return true;
    }
}
